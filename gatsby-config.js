module.exports = {
  pathPrefix: `/webpages`,
  siteMetadata: {
    title: `Zhiping Xu`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
