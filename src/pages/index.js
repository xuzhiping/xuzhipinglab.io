import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <p>Dr. Zhiping Xu received his B.S. (2002) and Ph.D (2007) from Tsinghua University. After working at Rice University (2007-2008) and MIT (2008-2010), he returned to Tsinghua in 2010 and is now a Professor of Engineering Mechanics. His research interests include the physics of synthetic materials and living systems in general. Currently his work is focused on the mechanical behaviors and transport processes of materials with complex microstructures.</p>
    <p>Our work is supported by the National Natural Science Fundation of China and industrial partners. For research activities, please review my <a href="http://scholar.google.com/citations?user=VNcqui8AAAAJ&hl=en">Google Scholar</a> and <a href="https://www.researchgate.net/profile/Zhiping_Xu7">ResearchGate</a> profiles. We host our research resources, including dissertations, presentations, lecture notes, source codes, computational and experimental data, to share with the science community, which could be accessed following this <a href="https://github.com/xuzpgroup">link</a>.</p>
    <p>My current group has six students - Shijun Wang, Wanzhen He, Shizhe Feng, Zian Zhang, Pengjie Shi, and Linxin Zhai. Former group members include Dr. Ke Zhou, now an Assistant Professor at Xian Jiaotong University, Dr. Shuping Jiao, now an Assistant Professor at Shanghai University, Dr. Enlai Gao, now an Associate Professor at Wuhan University, Dr. Yanlei Wang, now an Associate Professor at Institute of Process Engineering, CAS, Dr. Yu Wan, Dr. Cheng Chang, Dr. Zhigong Song, now a postdoc at Brown University, Dr. Anle Wang, now a Postdoc at Universität des Saarlandes, Dr. Bo Xie, Dr. Ning Wei, now a Professor at Jiangnan University, China, Dr. Chao Wang, now an Associate Professor at Institute of Mechanics, CAS, Dr. Yilun Liu, now a Professor at Xian Jiaotong University, Dr. Chao Chen, now a postdoc at University of Massachusetts at Amherst, Mr. Kai Jin, now a PhD Candidate at MIT.</p>
    <p>Mailing Address: Department of Engineering Mechanics, Tsinghua University, Beijing 100084, China.</p>
    <p>Email: xuzp at tsinghua.edu.cn</p>
  </div>
)

export default IndexPage
